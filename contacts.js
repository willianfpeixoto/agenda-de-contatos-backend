const clone = require('clone')
const config = require('./config')

const db = {}

const defaultData = {
  contacts: [
    {
      id: '1',
      name: 'Naruto Uzumaki',
      email: 'naruto@gmail.com',
      avatarURL: config.origin + '/naruto.jpg'
    },
    {
      id: '2',
      name: 'Coragem o cão covarde',
      email: 'coragem@gmail.com',
      avatarURL: config.origin + '/coragem.jpg'
    },
    {
      id: '3',
      name: 'Homer Simpsons',
      email: 'homer@gmail.com',
      avatarURL: config.origin + '/homer.jpg'
    },
    {
      id: '4',
      name: 'Mickey Mouse',
      email: 'mickey@gmail.com',
      avatarURL: config.origin + '/mickey.jpg'
    },
    {
      id: '5',
      name: 'Picapau',
      email: 'picapau@reacttraining.com',
      avatarURL: config.origin + '/picapau.jpg'
    }
  ]
}

const get = (token) => {
  let data = db[token]

  if (data == null) {
    data = db[token] = clone(defaultData)
  }

  return data
}

const add = (token, contact) => {
  if (!contact.id) {
    var newId = 0;
    get(token).contacts.forEach(c => {
      if(newId < c.id) newId = c.id;
    });
    newId++
    contact.id = newId
  }

  get(token).contacts.push(contact)

  return contact
}

const remove = (token, id) => {
  const data = get(token)
  const contact = data.contacts.find(c => c.id === id)

  if (contact) {
    data.contacts = data.contacts.filter(c => c !== contact)
  }

  return { contact }
}

module.exports = {
  get,
  add,
  remove
}
